﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GiantMove : MonoBehaviour
{
    public float moveSpeed = 10f;

    public float rotationSpeed = 2f;

    public GameObject cameraPivot;

    private float yaw = 0f;
    private float pitch = 0f;

    // Start is called before the first frame update
    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
    }

    // Update is called once per frame
    void Update()
    {
        float hAxis = Input.GetAxis("Horizontal");
        float vAxis = Input.GetAxis("Vertical");
        Vector3 direction = (transform.right * hAxis) + (transform.forward * vAxis);
        Vector3 movement = direction * moveSpeed * Time.deltaTime;

        float mouseX = Input.GetAxis("Mouse X");
        float mouseY = Input.GetAxis("Mouse Y");

        yaw += rotationSpeed * mouseX;
        pitch += rotationSpeed * mouseY;
        pitch = Mathf.Clamp(pitch, -40f, 50f);

        transform.eulerAngles = new Vector3(0f, yaw, 0f);
        cameraPivot.transform.eulerAngles = new Vector3(pitch, yaw, 0f);

        transform.position += movement;

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Cursor.lockState = CursorLockMode.None;
        }
    }
}
