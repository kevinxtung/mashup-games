﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Grabbable : MonoBehaviour
{
    private bool isConnected = false;

    public void SetConnected(bool connected)
    {
        isConnected = connected;
    }

    public bool IsConnected()
    {
        return isConnected;
    }
}
