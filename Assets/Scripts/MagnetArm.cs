﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagnetArm : MonoBehaviour
{
    public bool isLeft = false;

    private FixedJoint joint;

    private Grabbable currentGrabbable;

    void Update()
    {
        if (ShouldDeMagnetize())
        {
            BreakJoint();
        }
    }

    private void BreakJoint()
    {
        if (currentGrabbable != null && joint != null)
        {
            Destroy(joint);
            currentGrabbable.SetConnected(false);
            currentGrabbable = null;
        }
    }

    private bool ShouldDeMagnetize()
    {
        if (isLeft)
        {
            if (Input.GetKeyDown(KeyCode.Mouse0))
            {
                return true;
            }
        }
        else
        {
            if (Input.GetKeyDown(KeyCode.Mouse1))
            {
                return true;
            }
        }

        return false;
    }

    private bool ShouldStayDeMagnetized()
    {
        if (isLeft)
        {
            if (Input.GetKey(KeyCode.Mouse0))
            {
                return true;
            }
        }
        else
        {
            if (Input.GetKey(KeyCode.Mouse1))
            {
                return true;
            }
        }

        return false;
    }

    void OnCollisionEnter(Collision collision)
    {
        if (ShouldStayDeMagnetized()) return;

        if (collision.gameObject != null && collision.gameObject.TryGetComponent(out Grabbable grabbable))
        {
            if (!grabbable.IsConnected() && joint == null)
            {
                joint = gameObject.AddComponent<FixedJoint>();
                joint.connectedBody = collision.gameObject.GetComponent<Rigidbody>();
                grabbable.SetConnected(true);
                currentGrabbable = grabbable;
            }
        }
    }
}
