﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersonAgent : MonoBehaviour
{
    [SerializeField]
    private PersonState personState = PersonState.IDLE;
    private bool stateHasBeenOverriden = false;
    
    // This should be static, but it doesn't show up if you do that.
    [SerializeField]
    private GameObject giant;
    
    [SerializeField]
    private static float safteyRadius = 14.14f; // 14.14m roughly corresponds to 10m on the ground
    [SerializeField]
    private static float swapIdleAndWanderingProbability = 0.01f;

    // Do initialization of static fields here. No guarantee of when this will load. 
    static PersonAgent()
    {
    }

    private void Start()
    {
        PeopleController.Register(this);
    }

    public void Act()
    {
        if (personState == PersonState.IDLE)
        {
            // stand still
        }
        else if (personState == PersonState.WANDERING)
        {
            // walk around
        }
        else if (personState == PersonState.FLEEING)
        {
            // run fast
        }
        else if (personState == PersonState.DEAD)
        {
            PeopleController.Deregister(this);
            // do the die
        }

        SetNextState();
    }

    public void SetNextState()
    {
        if (personState == PersonState.DEAD || stateHasBeenOverriden)
        {
            return;
        }

        if (IsTooCloseToGiant())
        {
            personState = PersonState.FLEEING;
        }
        else
        {
            if (Random.value < swapIdleAndWanderingProbability)
            {
                if (personState == PersonState.WANDERING)
                {
                    personState = PersonState.IDLE;
                }
                else
                {
                    personState = PersonState.WANDERING;
                }
            }
        }
    }

    public void SetNextState(PersonState state)
    {
        if (personState == PersonState.DEAD)
        {
            return;
        }

        personState = state;
        stateHasBeenOverriden = true;
    }

    private bool IsTooCloseToGiant()
    {
        return Vector3.Distance(transform.position, giant.transform.position) < safteyRadius;
    }


    private void OnCollisionEnter(Collision collision)
    {
        personState = PersonState.DEAD;
    }
}
