﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PersonState
{
    IDLE,
    WANDERING,
    FLEEING,
    DEAD
}