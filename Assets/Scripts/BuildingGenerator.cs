﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingGenerator : MonoBehaviour
{
    public int numBuildings = 100;

    public float radius = 25f;

    public GameObject buildingPrefab;

    // Start is called before the first frame update
    void Start()
    {
        for (int i = 0; i < numBuildings; i++)
        {
            GameObject instance = Instantiate(buildingPrefab);
            instance.transform.localScale = Vector3.Scale(instance.transform.localScale, new Vector3(Random.Range(5f, 10f), Random.Range(1f, 10f), Random.Range(5f, 10f)));
            instance.transform.position = new Vector3(Random.Range(-radius, radius), instance.transform.localScale.y / 2, Random.Range(-radius, radius));
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
