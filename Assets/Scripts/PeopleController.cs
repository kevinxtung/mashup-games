﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class PeopleController : MonoBehaviour
{
    private static List<PersonAgent> personAgents = null;

    // Do initialization of static fields here. No guarantee of when this will load. 
    static PeopleController()
    {
    }

    // Start is called before the first frame update
    void Awake()
    {
        personAgents = new List<PersonAgent>();
    }

    public static void Register(PersonAgent personAgent)
    {
        Assert.IsTrue(!personAgents.Contains(personAgent));
        personAgents.Add(personAgent);
    }

    public static void Deregister(PersonAgent personAgent)
    {
        Assert.IsTrue(personAgents.Contains(personAgent));
        personAgents.Remove(personAgent);
    }

    // Update is called once per frame
    void Update()
    {
        foreach (PersonAgent person in personAgents)
        {
            person.Act();
        }
    }
}
